.SUFFIXES:	.pdf .svg

.svg.pdf:
	inkscape --export-pdf=$*.pdf $*.svg

FIGPATH = figs
CODEPATH = code

IMGS =	\

FIGS =	\
	$(FIGPATH)/by-nc-sa.pdf \
	$(FIGPATH)/os.pdf \
	$(FIGPATH)/mkernel.pdf \
	$(FIGPATH)/as.pdf \
	$(FIGPATH)/astmp.pdf \
	$(FIGPATH)/newkernelstack.pdf \
	$(FIGPATH)/x86_pagetable.pdf \
	$(FIGPATH)/xv6_layout.pdf \
	$(FIGPATH)/processlayout.pdf \
	$(FIGPATH)/intkstack.pdf \
	$(FIGPATH)/trapframe.pdf \
	$(FIGPATH)/race.pdf \
	$(FIGPATH)/switch.pdf \
	$(FIGPATH)/deadlock.pdf \
	$(FIGPATH)/fslayer.pdf \
	$(FIGPATH)/fslayout.pdf \
	$(FIGPATH)/inode.pdf \
	$(FIGPATH)/x86_translation.pdf \
	$(FIGPATH)/x86_seg.pdf \

CODE =	\
	$(CODEPATH)/ex_fork.c \
	$(CODEPATH)/ex_exec.c \
	$(CODEPATH)/ex_cat.c \
	$(CODEPATH)/ex_cat_2.c \
	$(CODEPATH)/ex_offset.c \
	$(CODEPATH)/ex_dup.c \
	$(CODEPATH)/ex_tube.c \
	$(CODEPATH)/ex_chemin_relatif.c \
	$(CODEPATH)/ex_chemin_absolu.c \
	$(CODEPATH)/ex_creation_fichier.c \
	$(CODEPATH)/stat.h \
	$(CODEPATH)/ex_link.c \
	$(CODEPATH)/ex_temp_inode.c \
	$(CODEPATH)/init_section_header \
	$(CODEPATH)/objdulp-f_kernel \
	$(CODEPATH)/ex_llist.c \
	$(CODEPATH)/ex_llist_lock.c \
	$(CODEPATH)/acquire.c \
	$(CODEPATH)/reorder.c \
	$(CODEPATH)/sendrecv1.c \
	$(CODEPATH)/sendrecv2.c \
	$(CODEPATH)/sendrecv3.c \
	$(CODEPATH)/sendrecv4.c \
	$(CODEPATH)/sleep-exo1.c \
	$(CODEPATH)/sleep-exo2.c \
	$(CODEPATH)/log.c \
	$(CODEPATH)/transaction.c \

SRC = \
	xv6-livret.tex \
	title.tex \
	acks.tex \
	unix.tex \
	first.tex \
	mem.tex \
	trap.tex \
	lock.tex \
	sched.tex \
	fs.tex \
	sum.tex \
	apphw.tex \
	appboot.tex \

XV6DIR = ./xv6-public/fmt

xv6-livret.pdf: $(SRC) $(IMGS) $(FIGS) $(CODE) xv6-public/xv6.pdf
	pdflatex xv6-livret
	./xv6line $(XV6DIR) xv6-livret.xve
	pdflatex xv6-livret
	# bibtex xv6-livret
	pdflatex xv6-livret

1:
	pdflatex xv6-livret

xv6-public/xv6.pdf:
	cd xv6-public && make xv6.pdf

index.html: README.md
	pandoc \
	    --standalone \
	    --metadata='pagetitle:Traduction du livret xv6' \
	    --variable='lang:fr' \
	    --variable='author-meta:Pierre David' \
	    --variable='author-meta:Timothée Zerbib' \
	    --variable='keywords:operating system' \
	    --variable='keywords:unix' \
	    --variable='highlighting-css: *{font-family:Arial,sans-serif;}' \
	    -o index.html \
	    README.md

clean:
	rm -f *.aux *.bbl *.blg *.log *.toc *.out *.xv[le]
	rm -f $(FIGS)
	rm -f xv6-livret.pdf
	cd xv6-public && make clean && rm -rf fmt xv6.pdf
