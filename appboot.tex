\chapter {Le chargeur d'amorçage}
  \label{app:boot}

Lorsqu'un PC x86 démarre, il commence par exécuter un programme appelé le BIOS
(\emph{Basic Input/Output System}), qui est stocké dans une mémoire non volatile
sur la carte mère.
Le rôle du BIOS est de préparer le matériel après quoi le chargeur d'amorçage
va transférer le contrôle au système d'exploitation.
Plus précisemment, il transfère le contrôle au code chargé depuis
le secteur de démarrage, le premier secteur de 512 octets du disque
de démarrage.
Le secteur de démarrage contient le chargeur d'amorçage~:
une suite d'instructions qui charge le noyau en mémoire.
Le BIOS charge le secteur de démarrage depuis l'adresse \adresse{0x7c00} puis
saute (place le registre processeur \registre{ip}) à cette adresse.
Lorsque le chargeur d'amorçage commence son exécution, le processeur simule
un processeur Intel 8088 et le rôle du chargeur d'amorçage est de mettre
le processeur dans un mode de fonctionnement plus moderne pour charger
le noyau xv6 du disque vers la mémoire puis transférer le contrôle au noyau.
Le chargeur d'amorçage de xv6 comprend deux fichiers sources, l'un écrit dans
une combinaison d'assembleur 16 et 32~bits (\fichier{bootasm.S}~;
[\xvline{bootasm.S:/^.include "asm.h"/}])
et l'autre écrit en C (\fichier{bootmain.c}~;
[\xvline{bootmain.c:/^.. Boot loader/}])).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code : Amorçage (en assembleur)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Code~: Amorçage (en assembleur)}
  \label{sec:code_amorcage_en_assembleur}

La première instruction du chargeur d'amorçage est
\codestyle{cli} [\xvline{bootasm.S:/cli.*interrupts/}], fonction qui désactive
les interruptions processeur.
Les interruptions sont un moyen pour le matériel de solliciter une fonction
du système d'exploitation nommée gestionnaire d'interruptions.
Le BIOS est un petit système d'exploitation, et il peut avoir instauré
son propre gestionnaire d'interruptions lorsqu'il initialisait le matériel.
Mais le BIOS n'est plus en cours d'exécution --- c'est le chargeur d'amorçage
qui s'exécute --- et il n'est donc plus approprié ou sûre de gérer
les interruptions des périphériques matériels.
Lorsque xv6 sera prêt (dans le chapitre \ref{chap:trap}), il va ré-activer
les interruptions.

Le processeur est en «~mode réel~», mode dans lequel il simule un Intel 8088.
En mode réel, il y a huit registres 16~bits à usage général, or le processeur
envoie des adresses 20~bits à la mémoire.
Les registres de segment \registre{cs}, \registre{ds}, \registre{es}
et \registre{ss} fournissent les bits supplémentaires nécessaires pour générer
des adresses mémoires 20~bits à partir de registres 16~bits.
Lorsqu'un programme fait référence à une adresse mémoire, le processeur ajoute
automatiquement 16 fois la valeur de l'un des registres de segment~;
ces registres ont une taille de 16~bits.
Le registre de segment à utiliser est génerallement implicite selon le type
de mémoire référencé~: la recherche d'instructions utilise \registre{cs},
la lecture et l'ecriture de données utilisent \registre{ds} et la lecture et
l'ecriture de la pile utilisent \registre{ss}.

\begin {figure}[t]
  \begin {center}
	  \includegraphics [width=10cm] {\figpath{x86_translation.pdf}}
  \end {center}
  \caption {Relations entre adresses logiques, linéaires et physiques.}
  \label{fig:x86_translation}
\end {figure}

Xv6 suppose qu'une instruction x86 utilise une adresse virtuelle pour
ses opérandes mémoires, or une instruction x86 utilise actuellement
une «~adresse logique~» (voir figure \ref{fig:x86_translation}).
Une adresse logique est constituée d'un selecteur de segment et d'un offset,
et est parfois notée \addrso{segment}{offset}.
La plupart du temps, le segment est implicite et le programme ne manipule
directement que l'offset.
Le processeur
\ndtshort{Dans le texte original, le terme \emph{segmentation hardware}
est employé, mais par soucis de simplicité, nous utiliserons «~processeur~».}
effectue les traductions décrite ci-dessus pour
générer une «~adresse linéaire~».
Si la MMU
\ndtshort{Dans le texte original, le terme \emph{paging hardware} est employé.
La pagination étant décrite dans le chapitre \ref{chap:mem}, nous utiliserons
directement le terme de MMU (\emph{Memory Managment Unit}), que nous accorderons
au féminin, la MMU représentant une unité.}
est activée (voir chapitre \ref{chap:mem}),
elle traduit les adresses linéaires en adresses physiques~;
dans le cas contraire, le processeur utilise les adresses linéaire comme
des adresses physiques.

Le chargeur d'amorçage n'active pas la pagination~; les adresses logiques
utilisées sont traduites en adresses linéaires par le processeur,
puis utilisées directement comme adresses physiques.
Xv6 configure le processeur pour traduire les adresses logiques
en adresses linéaires sans changements de sorte qu'elles soient toujours égales.
Pour des raisons hitoriques, nous avons utilisé le terme «~adresse virtuelle~»
pour faire référence aux adresses manipulées par les programmes~;
une adresse virtuelle xv6 est identique à une adresse logique x86 et est égale
à l'adresse linéaire qui lui est associée par le processeur.
Lorsque la pagination sera activée, la seule traduction d'adresse intéréssante
dans le système sera entre linéaire et physique.

Le BIOS ne fournit aucune garantie quant au contenu de \registre{ds},
\registre{es} et \registre{ss}, ainsi la priorité après avoir désactivé
les interruptions est de mettre la valeur du registre \registre{ax} à zéro
puis de copier ce zéro dans les registres \registre{ds}, \registre{es}
et \registre{ss} [\xvline{bootasm.S:/Set..ax.to.zero/,/Stack.Segment/}].

Un \addrso{segment}{offset} virtuel peut produire
une adresse physique de 21~bits, or le Intel 8088ne peut adresser que
des adresses mémoire de 20~bits, ainsi il écarte le bit de poids fort~:
\adresse{0xffff0}+\adresse{0xffff} = \adresse{0x10ffef},
mais l'adresse virtuelle \addrso{0xffff}{0xffff}
sur le 8088 fait référence à l'adresse physique \adresse{0x0ffef}.
Certains des premiers logiciels reposant sur le matériel ignoraient le 21ème
bit d'adresse, ainsi lorsqu'Intel a introduit des processeur avec plus de
20~bits d'adresse physique, IBM a fourni un hack de compatibilité qui est
indispensable pour tout matériel compatible PC.
Si le second bit du port de sortie contrôlleur de clavier vaut zéro,
le 21ème bit de l'adresse physique est toujours effacé~;
s'il vaut 1, le 21ème bit se comporte normallement.
Le chargeur d'amorçage doit activer le 21ème bir d'adresse en utilisant
les entrées/sorties au contrôlleur de clavier sur les ports 0x64 et 0x60
[\xvline{bootasm.S:/A20/,/outb.*.al,.0x60/}].

Les registres 16~bits d'usages généraux et de segment du mode réel rend
l'utilisation par un programme de plus de 65~536~octets de mémoire difficile
et il est impossible d'utiliser plus d'un megaoctet.
Les processeurs x86 possèdent, depuis le 80286, un «~mode protégé~», qui permet
aux adresses physiques d'avoir beaucoup plus de bits et, depuis le 80386,
un «~mode 32~bits~» qui permet aux registres, aux adresses virtuelles et à
la plupart des calculs arithmétiques d'être effectués avec 32~bits plutôt
que 16.
La séquence de démarrage de xv6 active le mode protégé et le mode 32~bits
de la façon suivante.

\begin {figure}[t]
  \begin {center}
	  \includegraphics [width=10cm] {\figpath{x86_seg.pdf}}
  \end {center}
  \caption {Segments en mode protégé.}
  \label{fig:x86_seg}
\end {figure}

En mode protégé, un registre de segment est un indexe vers
une «~table des descripteurs de segments~» (cf. figure \ref{fig:x86_seg}).
Chaque entrée de la table spécifie une adresse physique de base, une adresse
virtuelle maximale appelée la limite et les bits d'authorisation pour
le segment.
Ces permissions représentent les protections en mode protégé~: le noyau peut
les utiliser pour s'assurer qu'un programme n'utilise que
son propre espace mémoire.

Xv6 n'utilise pratiquement pas les segments~; il utilise à la place
la pagination, comme le décrit le chapitre \ref{chap:mem}.
Le chargeur d'amorçage instaure la table des descripteurs de segments
\codestyle{gdt} \xvline{bootasm.S:/^gdt:/,/data.seg/} de sorte que tous
les segments aient une adresse de base de zéro et la limite maximale possible
(quatre gigaoctets).
La table possède une entrée nulle, une entrée pour le code exécutable et
une entrée pour les données.
Le descripteur de segment du code a un indicateur permettant de savoir si
le code doit s'exécuter en mode 32~bits \xvline{asm.h:/SEG.ASM/}.
Avec cette configuration, lorsque le chargeur d'amorçage entre dans
le mode protégé, les adresses logiques correspondent unes à unes aux
adresse physiques.

Le chargeur d'amorçage exécute une instruction
\codestyle{lgdt} [\xvline{bootasm.S:/lgdt/}] pour charger le registre GDT
(\emph{Global Descriptor Table}) avec la valeur
\codestyle{gdtdesc} [\xvline{bootasm.S:/^gdtdesc:/,/address.gdt/}],
qui pointe sur la table \codestyle{gdt}.

Une fois le registre GDT chargé, le chargeur d'amorçage active le mode protégé
en mettant la valeur 1~bit (\codestyle{CR0\_PE}) dans le registre \registre{cr0}
[\xvline{bootasm.S:/movl.*.cr0/,/movl.*,..cr0/}].
Activer le mode protégé ne change pas immédiatement la façon dont le processeur
traduit les adresses logiques en adresses physiques~; ce n'est que lorsque l'on
charge une nouvelle valeur dans un registre de segment que le processeur lit
le registre GDT et modifie ses paramètres de segmentation internes.
On ne peut pas modifier directement \registre{cs}, ainsi à la place, on exécute
une instruction \codestyle{ljump} (\emph{far jump}) [\xvline{bootasm.S:/ljmp/}],
qui permet de spécifier un selecteur de segment code.
Le jump continue son exécution à la ligne suivante
[\xvline{bootasm.S:/^start32/}], et ce faisant, définit \registre{cs} comme
faisant référence à l'entrée descripteur de code dans \codestyle{gdt}.
Ce descripteur décrit un segment code 32~bits, ainsi le processeur commute
en mode 32~bits.
Le chargeur d'amorçage a permit au processeur d'évoluer depuis le 8088 vers
le 80286 puis le 80386.

La première action du chargeur d'amorçage en mode 32~bits est d'initialiser
les registres de segments données avec la valeur
\codestyle{SEG\_KDATA} [\xvline{bootasm.S:/movw.*SEG.KDATA/,/Stack.Segment/}].
Les adresses logiques correspondent maintenant directement aux
adresses physiques.
La seule étape à effectuer avant d'exécuter du code C est d'instaurer une pile
dans une région inutilisée de la mémoire.
La mémoire entre de l'adresse \adresse{0xa0000} jusqu'à \adresse{0x100000}
est générallement parsemée de zones de mémoire de périphériques, et le noyau
de xv6 s'attend à être situé à l'adresse \adresse{0x100000}.
Le chargeur d'amorçage lui est situé entre les adresses \adresse{0x7c00} et
\adresse{0x7e00} (512~octets).
N'importe quelle autre section de la mémoire seraient fondamentalement un bon
emplacement pour la pile.
Le chargeur d'amorçage choisit l'adresse \adresse{0x7c00} (connu dans ce fichier
comme \codestyle{\$start}) comme sommet de la pile~; la pile décroit à partir
d'ici jusqu'à atteindre l'adresse \adresse{0x0000}, loin du chargeur d'amorçage.

Enfin, le chargeur d'amorçage appelle la fonction C
\xvfct{bootmain}{bootasm.S:/call.*bootmain/}.
Le rôle de bootmain est de charger et d'exécuter le noyau.
Cette fonction ne retourne que si quelque chose s'est mal passé.
Dans ce cas, le code envoie quelques mots en sortie sur le port \adresse{0x8a00}
[\xvline{bootasm.S:/bootmain.returns/,/spin:/-1}].
Sur du vrai matériel, il n'y a pas de périphériques connecté à ce port,
ainsi ce code ne fait rien.
Si le chargeur d'amorçage s'exécute à l'intérieur d'un simulateur de PC,
le port \adresse{0x8a00} est connecté au simulateur lui-même et peut lui rendre
le contrôle.
Simulateur ou non, le code exécute ensuite une boucle infinie
[\xvline{bootasm.S:/^spin:/,/jmp/}].
Un vrai chargeur d'amorçage pourrait tenter d'afficher un message
d'erreur avant.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code : Amorçage (en C)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Code~: Amorçage (en C)}
  \label{sec:code_amorcage_en_c}

La partie en C du chargeur d'amorçage,
\fichier{bootmain.c} [\xvline{bootmain.c:1}],
s'attend à trouver une copie de l'exécutable du noyau sur le disque,
à partir du deuxième secteur.
Le noyau est un binaire au format ELF, comme vu dans le chapitre \ref{chap:mem}.
Pour avoir accès à l'en-tête ELF, \xvf{bootmain} charge les premiers 4096~octets
du binaire ELF [\xvline{bootmain.c:/readseg..uchar..elf/}].
Il place la copie en mémoire à l'adresse \adresse{0x10000}.

\ndtlong{
  Ce paragraphe a été supprimé de la version originale du livret,
  je le replace ici à titre informatif.
  
  \xvf{Bootmain} effectue beaucoup de casts entre pointeurs et entiers, et entre
  différents types de pointeurs
  [\xvline{bootmain.c:/elfhdr..0x10000/}\xvlinesty{, }\xvline{bootmain.c:/readseg!(!(/}\xvlinesty{, and so on}].
  Ces casts n'ont de sens que si le compilateur et le processeur représentent
  les entiers et tous les pointeurs essentiellement de la même manière,
  ce qui n'est pas vrai sur toutes les combinaisons de machines/compilateurs.
  Cela est en revanche vrai pour le x86 en mode 32~bits~:
  les entiers sont encodés en 32~bits et tous les pointeurs sont des adresses
  également sur 32~bits.
}

La prochaine étape est une vérification rapide qu'il s'agisse probablement bien
d'un binaire ELF, et non d'un disque non initialisé.
\xvf{Bootmain} lit le contenu de la section commençant à l'emplacement disque
\codestyle{off}~octets après le début de l'en-tête ELF,
et écrit à l'adresse \codestyle{paddr}.
\xvf{Bootmain} appelle \xvf{readseg} pour charger depuis le disque
[\xvline{bootmain.c:/readseg.*filesz/}] et appelle \xvf{stosb}
pour mettre à zéro le reste du segment [\xvline{bootmain.c:/stosb/}].
\xvfct{Stosb}{x86.h:/^stosb/} utilise l'instruction x86 \codestyle{rep stosb}
pour initialiser chaque octet d'un bloc de la mémoire.

\ndtlong{
  Cette partie a été supprimée de la version originale du livret,
  je la replace ici à titre informatif.
  
  \xvfct{Readseg}{bootmain.c:/^readseg/} lit au minimum \codestyle{count}~octets
  depuis la position \codestyle{offset} du disque, vers la mémoire à l'adresse
  \codestyle{pa}.
  L'interface de disque IDE du x86 fonctionne sur des secteurs de 512~octets,
  ainsi \xvf{readseg} peut lire non seulement la section mémoire désirée,
  mais aussi quelques octets avant et après, dépendant de l'alignement.
  Pour le segment de programme de l'example ci-dessus, le chargeur d'amorçage
  va appeler \codestyle{readseg((uchar*)0x100000, 0xb57e, 0x1000)}.
  \xvf{Readseg} commence par évaluer l'adresse physique de fin, la première
  adresse supérieur à \codestyle{paddr} qui ne nécesssite pas d'être chargée
  depuis le disque [\xvline{bootmain.c:/epa.=/}],
  et arrondit à l'inférieur \codestyle{pa} à un décalage aligné sur
  les secteur du disque.
  Puis il convertit le décalage d'un décalage exprimé en octets
  à un décalage exprimé en secteurs~; il ajoute 1 car le noyau commence
  au secteur disque 1 (le secteur disque 0 correspond au chargeur d'amorçage).
  Enfin, il appelle \xvf{readsect} pour lire chaque secteur vers la mémoire.

  \xvfct{Readsect}{bootmain.c:/^readsect/} lit un unique secteur disque.
  \xvf{Readsect} commence par appeler \xvf{waitdisk} pour attendre
  jusqu'à ce que le disque signale qu'il est prêt à accepter des commandes.
  Le disque se signale en mettant les deux bits de poids forts
  de son octet d'état (connecté au port d'entré \adresse{0x1F7})
  à \codestyle{01}.
  \xvfct{Waitdisk}{bootmain.c:/^waitdisk/} lit l'octet d'état jusqu'à ce que
  les bits aient les bonnes valeurs.
  Le chapitre \ref{chap:trap} utilise une manière efficace d'attendre
  les changements de status du matériel, mais le \emph{polling},
  tel qu'effetué ici, est convenable pour le chargeur d'amorçage.

  Une fois le disque prêt, \xvf{readsect} effectue une commande de lecture.
  Il commence par écrire les arguments de la commande --- le nombre de secteurs
  et le numéro de secteur (offset) --- sur les registres du disque
  sur les ports de sortie \adresse{0x1F2-0x1F6}
  [\xvline{bootmain.c:/1F2/,/1F6/}].
  Le bit \codestyle{0xe0} écrit sur le port \adresse{0x1f6} signale au disque
  que \codestyle{0x1f3-0x1f6} contient un numéro de secteur
  (aussi appelé adresse de bloc linéaire) en opposition avec
  une adresse de cylindre/tête/secteur plus compliquée, utilisée dans
  les disques PC plus anciens.
  Après avoir écrit les arguments, \xvf{readsect} écrit au registre de commande
  pour déclencher la lecture [\xvline{bootmain.c:/0x1F7/}].
  La commande \codestyle{0x20} correspond à «~lecture de secteurs~».
  À partir d'ici, le disque va lire les données stoquées dans
  les secteurs spécifiés et les rendre accessibles par morceaux de 32~bits
  sur le port d'entré \codestyle{0x1f0}.
  \xvfct{Waitdisk}{bootmain.c:/^waitdisk/} attend jusqu'à ce que le disque
  signale que les données sont prêtes, puis l'appel à \codestyle{insl}
  lit les 128 (\codestyle{SECTSIZE/4}) morceaux 32~bits vers la mémoire
  commençant à l'adresse \codestyle{dst} [\xvline{bootmain.c:/insl.0x1F0/}].

  \xvf{Inb}, \xvf{outb} et \xvf{insl} ne sont pas des fonctions C ordinaires.
  Il s'agit de fonctions inline dont le corps comprend des morceaux
  en langage assembleur
  [\xvline{x86.h:/^inb/}\xvlinesty{, }\xvline{x86.h:/^outb/}\xvlinesty{, }\xvline{x86.h:/^insl/}].
  Lorsque \codestyle{gcc} (le compilateur C qu'utilise xv6) voit les appels
  à \xvfct{inb}{bootmain.c:/inb!(/}, l'assembleur intégré provoque l'émission
  d'une seule instruction \codestyle{inb}.
  Ce style permet d'utiliser des instruction bas niveaux comme \codestyle{inb}
  et \codestyle{outb} alors que l'on écrit toujours la logique en C plutôt
  qu'en assembleur.

  L'implémentation de \xvfct{insl}{x86.h:/^insl/} mérite de s'attarder dessus.
  \codestyle{Rep insl} est en réalité une petite boucle déguisée
  en une simple instruction.
  Le préfixe \codestyle{rep} exécute les instructions suivantes
  \registre{ecx} fois, décrémentant \registre{ecx} après chaque itérations.
  L'instruction \codestyle{insl} lit des valeurs 32~bits depuis le port
  \registre{dx} vers la mémoire à l'adresse \registre{edi}
  puis incrémente \registre{edi} de 4.
  Ainsi, \codestyle{rep insl} copie 4$\times$\registre{ecx}~octets,
  dans des morceaux 32~bits, depuis le port \registre{dx} vers la mémoire
  commençant à l'adresse \registre{edi}.
  Les annotations du registre indiquent à \codestyle{gcc} de se préparer
  à la séquence assembleur en stockant \codestyle{dst} dans le registre
  \registre{edi}, \codestyle{cnt} dans \registre{ecx} et \codestyle{port}
  dans \registre{dx}.
  Ainsi, la fonction \xvf{insl} copie les 4$\times$\codestyle{cnt}~octets
  depuis le port 32~bits \codestyle{port} vers la mémoire commençant
  à \codestyle{dst}.
  L'instruction \codestyle{cld} nettoie l'indicateur de direction
  du processeur, de sorte que l'instruction \codestyle{insl} incrémente
  \registre{edi}~; lorsque l'indicateur est activé, \codestyle{insl} décrémente
  \registre{edi} à la place.
  Les conventions d'appels x86 ne définissent pas l'état
  de l'indicateur de direction à l'entrée d'une fonction,
  ainsi chaque utilisation d'une instruction \codestyle{insl} doit
  l'initialiser à la valeur souhaitée.

  Le chargeur d'amorçage est presque terminé.
  \xvf{Bootmain} boucle en appelant \xvf{readseg} qui boucle en appelant
  \xvf{readsect} [\xvline{bootmain.c:/for.;/,/stosb/+1}].
  À la fin de la boucle, \xvf{bootmain} a chargé le noyau dans la mémoire.
}

La compilation et l'édition de lien du noyau sont tels que le noyau s'attend
à se trouver à l'adresse virtuelle commençant à \adresse{0x80100000}.
Ainsi, les instructions d'appel de fonctions doivent mentionner des adresses
de destination du type \adresse{0x801xxxxx}~; des exemples se trouvent dans
\fichier{kernel.asm}.
Cette adresse est configurée dans \fichier{kernel.ld}
[\xvline{kernel.ld:/0x80100000/}].
\adresse{0x80100000} est une adresse relativement haute, située vers la fin
de l'espace d'adressage de 32~bits~; le chapitre \ref{chap:mem} explique
les raisons de ce choix.
Il pourrait ne pas y avoir de mémoire physique à une si haute adresse.
Une fois que le noyau commence son exécution, il va initialiser la MMU
pour quelle lie les adresses virtuelles comma,çant à \adresse{0x80100000}
aux adresses physiques commançant à \adresse{0x00100000}~;
le noyau suppose qu'il existe une adresse physique à une si petite adresse.
À ce moment du processus d'amorçage cependant, la MMU n'est pas activée.
À la place, \fichier{kernel.ld} spécifie que \codestyle{paddr} de l'ELF
commence à l'adresse \adresse{0x00100000}, ce qui fait que
le chargeur d'amorçage copie le noyau à une adresse basse, à laquelle la MMU
va éventuellement faire référence.

La dernière étape du chargeur d'amorçage est d'appeler le point d'entrée noyau,
qui est l'instruction par laquelle le noyau s'attend à commencer son exécution.
Pour xv6, l'adresse du point d'entrée est \adresse{0x10000c}~:

\code{objdulp-f_kernel}

Par convention, le symbole \codestyle{\_start} spécifie le point d'entré ELF,
qui est définit dans \fichier{entry.S} [\xvline{entry.S:/^_start/}].
Puisque xv6 n'a pas initialiser de mémoire virtuelle pour l'instant,
le point d'entrée de xv6 est l'adresse physique de \xvf{entry}
[\xvline{entry.S:/^entry/}].


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dans la réalité
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Dans la réalité}
  \label{sec:dans_la_realite_appboot}

Le chargeur d'amorçage décrit dans cette annexe compile autour de 470~octets
de code machine, dépendant des optimisations utilisées durant la compilation
du code C.
Afin de tenir dans cette petite quantité de mémoire, le chargeur d'amorçage
de xv6 effectue l'hyptohèse de simplification majeure que le noyau a été écrit
sur le disque de démarrage, de manière contigüe, et qu'il commence au secteur 1.
De façon plus commune, les noyaux sont stoqués dans des systèmes de fichiers
ordinaires où ils peuvent ne pas être contigüe, ou sont chargés
depuis le réseau.
Ces complications nécessitent que le chargeur d'amorçage soit capable
de gérer toute une palette de contrôleur de disque et réseau et de comprendre
plusieurs systèmes de fichiers et protocols réseaux.
En d'autres termes, le chargeur d'amorçage lui-même doit être
un système d'exploitation miniature.
Puisqu'un chargeur d'amorçage aussi complexe ne tiendrait sûrement pas
en 512~octets, la plupart des systèmes d'exploitation des PC utilisent
un processus de démarrage en deux temps.
Premièrement, un chargeur d'amorçage simple, comme celui montré
dans cette annexe, charge un chargeur d'amorçage plus complet depuis
un emplacement connu du disque, s'appuyant souvent sur le BIOS pour accéder
au disque plutôt que d'essayer d'y accéder
\ndtshort{Et pour ce faire, devoir savoir utiliser des pilotes.}
lui même.
Puis, le chargeur d'amorçage complet, libéré de la limite des 512~octets,
peut implémenter la compléxité nécessaire pour trouver, charger et exécuter
le noyau désiré.
Les PC modernes évitent beaucoup des difficultés citées car ils supportent
le format UEFI (\emph{Unified Extensible Firmware Interface}), qui permet
aux PC de lire un chargeur d'amorçage plus lourd depuis le disque
(et de l'exécuter en mode protégé ou en mode 32~bits).

Cette annexe est écrite comme si la seule chose qui se produisait entre
l'allumage et l'exécution du chargeur d'amorçage était que le BIOS chargait
le secteur de démarrage.
En réalité, le BIOS effectue un nombre considérable d’initialisations
afin que le matériel complexe d’un ordinateur moderne ressemble
à un PC standard traditionnel.
Le BIOS est vraiment un système d'exploitation miniature embarqué
dans le matériel, qui s'exécute après le démarrage de l'ordinateur.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Exercices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Exercices}
  \label{sec:exercices_appboot}

\begin{enumerate}
  \item{
    En raison de la granularité, l'appel à \codestyle{readseg} dans le code
    est équivalent à \codestyle{readseg((uchar*)0x100000, 0xb500, 0x1000)}.
    En pratique, ce mauvais comportement ne pose pas de problème.
    Pourquoi le mauvais \codestyle{readsect} ne pose-t-il pas de problème ?
  }
  \item{
    Supposons que nous voulions que \codestyle{bootmain} charge le noyau
    à l'adresse \adresse{0x200000} au lieu de \adresse{0x100000}, et que nous
    ayons réalisé ceci en modifiant \codestyle{bootmain} pour ajouter
    \codestyle{0x100000} aux adresses virtuelles de chaque section ELF.
    Quelque chose ne fonctionnerait pas.
    Quoi ?
  }
  \item{
    Il paraît potentiellement dangereux pour le chargeur d'amorçage de copier
    l'en-tête ELF en mémoire, à l'emplacement arbitraire \adresse{0x100000}.
    Pourquoi ne pas appeler \codestyle{malloc} pour obtenir
    l'espace mémoire necéssaire ?
  }
\end{enumerate}
