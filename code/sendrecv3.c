  300	struct q {
  301	  struct spinlock lock;
  302	  void *ptr;
  303	};
  304	
  305	void*
  306	send(struct q *q, void *p)
  307	{
  308	  acquire(&q->lock);
  309	  while(q->ptr != 0)
  310	    ;
  311	  q->ptr = p;
  312	  wakeup(q);
  313	  release(&q->lock);
  314	}
  315	
  316	void*
  317	recv(struct q *q)
  318	{
  319	  void *p;
  320	
  321	  acquire(&q->lock);
  322	  while((p = q->ptr) == 0)
  323	    sleep(q);
  324	  q->ptr = 0;
  325	  release(&q->lock);
  326	  return p;
  327	}
